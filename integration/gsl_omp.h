/* integration/gsl_omp.h
 * 
 * Copyright (C) 2017 Vincent Danjean, Edouard Marguerite
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __GSL_OMP_H__
#define __GSL_OMP_H__

#ifdef GSL_OMP
#  include <omp.h>
#  define GSL_OMP_NAME(name) name##_omp
#else
#  define GSL_OMP_NAME(name) name
#endif

#ifdef GSL_OMP
typedef struct {
  int dynamic;
  int nested;
} gsl_omp_var_t;
#  define gsl_omp_init() \
  gsl_omp_var_t __gsl_omp_var __attribute__((cleanup(__gsl_omp_cleanup)));\
  __gsl_omp_init(&__gsl_omp_var);

static inline
void __gsl_omp_init(gsl_omp_var_t *p_gsl_omp_var) {
  int dynamic=omp_get_dynamic();
  p_gsl_omp_var->dynamic=dynamic;
  if (!dynamic) {
	omp_set_dynamic(1); // Explicitly enable dynamic teams
  }
  int nested=omp_get_nested();
  p_gsl_omp_var->nested=dynamic;
  if (!nested) {
	omp_set_nested(1); // Explicitly enable nested parallelism
  }
}

static inline
void __gsl_omp_cleanup(gsl_omp_var_t *p_gsl_omp_var) {
  int dynamic=p_gsl_omp_var->dynamic;
  if (!dynamic) {
	omp_set_dynamic(0); // Restaure old state
  }
  int nested=p_gsl_omp_var->dynamic;
  if (!nested) {
	omp_set_nested(0); // Restaure old state
  }
}
#else
#  define gsl_omp_init() ((void)0)
#endif

#endif



#define GSL_OMP

#include "gsl_omp.h"

#define gsl_integration_qk15(f,a,b,r,ae,ra,re) \
  GSL_OMP_NAME(gsl_integration_qk15)(f,a,b,r,ae,ra,re)
#define gsl_integration_qk21(f,a,b,r,ae,ra,re) \
  GSL_OMP_NAME(gsl_integration_qk21)(f,a,b,r,ae,ra,re)
#define gsl_integration_qk31(f,a,b,r,ae,ra,re) \
  GSL_OMP_NAME(gsl_integration_qk31)(f,a,b,r,ae,ra,re)
#define gsl_integration_qk41(f,a,b,r,ae,ra,re) \
  GSL_OMP_NAME(gsl_integration_qk41)(f,a,b,r,ae,ra,re)
#define gsl_integration_qk51(f,a,b,r,ae,ra,re) \
  GSL_OMP_NAME(gsl_integration_qk51)(f,a,b,r,ae,ra,re)
#define gsl_integration_qk61(f,a,b,r,ae,ra,re) \
  GSL_OMP_NAME(gsl_integration_qk61)(f,a,b,r,ae,ra,re)
#define gsl_integration_qng(f,a,b,ae,re,r,era,n) \
  GSL_OMP_NAME(gsl_integration_qng)(f,a,b,ae,re,r,era,n)
#define gsl_integration_qag(f,a,b,ae,re,l,k,w,r,era) \
  GSL_OMP_NAME(gsl_integration_qag)(f,a,b,ae,re,l,k,w,r,era)
#define gsl_integration_qags(f,a,b,ae,re,l,w,r,era) \
  GSL_OMP_NAME(gsl_integration_qags)(f,a,b,ae,re,l,w,r,era)
#define gsl_integration_qagp(f,p,n,ae,re,l,w,r,era) \
  GSL_OMP_NAME(gsl_integration_qagp)(f,p,n,ae,re,l,w,r,era)
#define gsl_integration_qagi(f,ae,re,l,w,r,era) \
  GSL_OMP_NAME(gsl_integration_qagi)(f,ae,re,l,w,r,era)
#define gsl_integration_qagil(f,b,ae,re,l,w,r,era) \
  GSL_OMP_NAME(gsl_integration_qagil)(f,b,ae,re,l,w,r,era)
#define gsl_integration_qagiu(f,a,ae,re,l,w,r,era) \
  GSL_OMP_NAME(gsl_integration_qagiu)(f,a,ae,re,l,w,r,era)
#define gsl_integration_qawc(f,a,b,c,ae,re,l,w,r,era) \
  GSL_OMP_NAME(gsl_integration_qawc)(f,a,b,c,ae,re,l,w,r,era)
#define gsl_integration_qawf(f,a,ae,l,w,cw,wf,r,era) \
  GSL_OMP_NAME(gsl_integration_qawf)(f,a,ae,l,w,cw,wf,r,era)
#define gsl_integration_qawo(f,a,ae,re,l,w,wf,r,era) \
  GSL_OMP_NAME(gsl_integration_qawo)(f,a,ae,re,l,w,wf,r,era)
#define gsl_integration_qaws(f,a,b,t,ae,re,l,w,r,era) \
  GSL_OMP_NAME(gsl_integration_qaws)(f,a,b,t,ae,re,l,w,r,era)

